exports.imgDomain = 'http://127.0.0.1:7001'

// 数据库配置
exports.mongoose = {
  client: {
    url: 'mongodb://127.0.0.1:27017/bnm_Web',
    options: {
      // useNewUrlParser: true,
      useUnifiedTopology: true
    },
    plugins: [],
  }
};