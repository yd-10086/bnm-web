'use strict';

// swagger 接口文档
// exports.swaggerdoc = {
//   enable: true,
//   package: 'egg-swagger-doc',
// }

//模板引擎
exports.nunjucks = {
  enable: true,
  package: 'egg-view-nunjucks',
};

// mongoose
exports.mongoose = {
  enable: true,
  package: 'egg-mongoose'
}

// 参数校验
exports.validate = {
  enable: true,
  convert: true,
  package: 'egg-validate'
}

//解决跨域
exports.cors = {
  enable: true,
  package: 'egg-cors'
}