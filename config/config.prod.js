const path = require('path')

exports.logger = {
  level: 'INFO',
  dir: path.join(__dirname, '../logs/prod/app') // 保存路径为工程路径下`logs/prod/app`
};

exports.imgDomain = 'http://120.25.100.147:7001'

// 数据库配置
exports.mongoose = {
  client: {
    url: 'mongodb://120.25.100.147:27017/bnm_Web',
    options: {
      auth: { authSource: "admin" },
      user: 'mongo',
      pass: 'system888888',
      // useNewUrlParser: true,
      useUnifiedTopology: true
    },
    plugins: [],
  }
};