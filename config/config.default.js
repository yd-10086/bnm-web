/* eslint valid-jsdoc: "off" */

'use strict';
const path = require('path')

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};
  
  // add your user config here
  const userConfig = {
    // myAppName: '',
    
  };


  // token秘钥
  config.jwt = {
    secret: '5ea9521a-15ce-488f-8e32-edba9b1ae09e',
    expiresIn: '1d'
  }


  // 文件上传目录
  config.uploadDir = 'app/public/images/upload'

  // 上传文件大小
  config.multipart = {
    mode: 'stream',
    fileSize: '200mb',
    fileExtensions: [ '.rar','.zip','.doc','.docx','.ppt','.txt','.xlsx','.xls','.pdf' ], // 扩展允许接收的文件后缀
  }
  
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1636812084385_3991';

  // add your middleware config here
  config.middleware = ['auth','errorHandler','frontAuth'];


  // 模板引擎
  config.view = {
    defaultExtension: '.html',
    defaultViewEngine: 'nunjucks',
    mapping: {
      '.html': 'nunjucks',
    },
  };

  // // 数据库配置
  // config.mongoose = {
  //   client: {
  //     url: 'mongodb://120.25.100.147:27017/bnm_Web',
  //     options: {
  //       auth: { authSource: "admin" },
  //       user: 'mongo',
  //       pass: 'system888888',
  //       // useNewUrlParser: true,
  //       useUnifiedTopology: true
  //     },
  //     plugins: [],
  //   }
  // };

  // 配置 session
  config.session = {
    key: 'EGG_SESS',
    maxAge: 24 * 3600 * 1000, // 1 天 
    httpOnly: true,
    encrypt: true,
    renew: true,
  };

  

  config.cluster = {
    listen: {
      path: '',
      port: 8014,
      hostname: '0.0.0.0',
    }
  }

  // 配置跨域设置
  config.security = {
    csrf: {
      enable: false
    },
    domainWhiteList: ['http://120.25.100.147','http://127.0.0.1:7002',]  // 白名单列表
  }
  config.cors = {
    origin: '*'
    // {string|Function} origin: '*',
    // {string|Array} allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH'
  };

  return {
    ...config,
    ...userConfig,
  };
};
