'use strict';
const baseArticle = {
  title: {type: 'string',description: '标题'},
  description: {type: 'string',description: '摘要'},
  content: {type: 'string',description: '内容'},
}
const baseMenu = {
  parentId: {type: 'string',description: '父级_id'},
  title: {type: 'string',required: true, description: '菜单名称'},
  path: {type: 'string',required: true, description: '菜单路由'},
  sortNum: {type: 'number',description: '排序'},
}
module.exports = {
  login: {
    username: {type:'string', required:true, description:'登录账号',example:'admin123'},
    password: {type:'string', required:true, description:'登录密码', example: '123'},
  },
  register: {
    nickname: {type: 'string',required: false, description: '用户昵称', example: '张三'},
    username: {type:'string', required:true, description:'登录账号',example:'admin123'},
    password: {type:'string', required:true, description:'登录密码', example: '123'},
    confirmPassword: {type:'string', required:true, description:'确认密码', example: '123'},
  },
  file: {
    parameters: { name: 'file',in: 'formData',type: 'file',contentType: 'multipart/*' }
  },
  deleteFile: {
    id: {type: 'string',required: true, description: '文件_id'}
  },
  insertArticle: baseArticle,
  titleRequest: {
    title: {type: 'string',required: true, description: 'title'},
    sortNum: {type: 'number',required: true, description: 'sortNum'},
  },
  updateTitleRequest: {
    id: {type: 'string',required: true,description: '_id'},
    sortNum: {type: 'number',required: true, description: 'sortNum'},
    title: {type: 'string',required: true, description: 'title'}
  },
  updateArticle: {
    id: {type: 'string',required: true,description: '文章_id'},
    ...baseArticle
  },
  insertMessage: {
    target: {type: 'string',required: true, description: '目标对象'},
    content: { type: 'string',required: true, }
  },
  systemRequest: {
    name: {type: 'string', description: '博客名'},
    qq: {type: 'string', description: 'QQ'},
    wx: {type: 'string', description: '微信'},
    avatar: {type: 'string', description: '头像'},
    motto: {type: 'string', description: '座右铭'},
    signature: {type: 'string', description: '个性签名'},
    aboutMe: {type: 'string', description: '关于我'},
    seoKeywords: {type: 'string', description: 'seo关键字'},
    seoDescription: {type: 'string', description: 'seo描述'},
  },
  insertMenu: baseMenu,
  updateMenu: {
    id: {type: 'string',required: true,description: '菜单_id'},
    ...baseMenu
  }
}