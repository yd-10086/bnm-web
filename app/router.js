'use strict';
const backend = require('./router/backend')
/**
 * @param {Egg.Application} app - egg application
 */

module.exports = app => {
  const { router, controller } = app;
  const auth = app.middleware.auth()
  const frontAuth = app.middleware.frontAuth()

  router
    .get('/', controller.homeController.index) // 首页页面·
    .get('/about', controller.homeController.about) // 关于页面
    .get('/innovate', controller.homeController.innovate) // 研发创新
    .get('/contact-us', controller.homeController.contactUs) // 联系我们
    .get('/products', controller.homeController.products) // 产品页
    .get('/product-details/:id', controller.homeController.productDetail) // 产品详情页
    .get('/program', controller.homeController.program) // 解决方案页
    .get('/program-details/:id', controller.homeController.programDetail) // 解决方案详情页
    .get('/support', controller.homeController.support) // 服务支持页
    .get('/message', controller.homeController.message) // 网上留言页
    .get('/download', controller.homeController.download) //资料下载页
    .get('/after-sale', controller.homeController.afterSale) //售后服务页
    .get('/serve-survey', controller.homeController.serveSurvey) //满意度调研页
    .get('/number-future', controller.homeController.numberFuture) //数字未来页
    .get('/news', controller.homeController.news) //新闻页面
    .get('/video-center', controller.homeController.videoCenter) //满意度调研
    .get('/notice', controller.homeController.notice) // 公司公告页
    
  // 管理后台接口
  require('./router/backend')(app);
  // 重定向到swagger-ui.html
  // router.redirect('/', '/home', 302);
  // router.redirect('/swagger', '/swagger-ui.html', 302);
};
