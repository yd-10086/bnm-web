'use strict';

module.exports = function(app) {
  const { router, controller } = app;
  const auth = app.middleware.auth()
  router.post('/api/v1/user/login', controller.userController.login)  //登录
  router.post('/api/v1/user/register', controller.userController.register) //注册
  router.get('/api/v1/user/list', controller.userController.list) //用户列表
  router.post('/api/v1/user/updatePassword', controller.userController.updatePassword) //修改密码

  // router.get('/api/v1/banner/list', app.middleware.auth({ required: false }), controller.bannerController.list) // 查询banner
  // router.post('/api/v1/banner/insert', auth, controller.bannerController.insert) // 添加banner
  // router.post('/api/v1/banner/delete', auth, controller.bannerController.delete) // 删除banner
  // router.post('/api/v1/banner/update', auth, controller.bannerController.update) // 修改banner
  // router.get('/api/v1/technology/list', app.middleware.auth({ required: false }), controller.technologyController.list) // 查询技术支持
  // router.post('/api/v1/technology/insert', auth, controller.technologyController.insert) // 添加技术支持
  // router.post('/api/v1/technology/delete', auth, controller.technologyController.delete) // 删除技术支持
  // router.post('/api/v1/technology/update', auth, controller.technologyController.update) // 修改技术支持
  // router.get('/api/v1/company/list', app.middleware.auth({ required: false }), controller.companyController.list) // 查询子公司
  // router.post('/api/v1/company/insert', auth, controller.companyController.insert) // 添加子公司
  // router.post('/api/v1/company/delete', auth, controller.companyController.delete) // 删除子公司
  // router.post('/api/v1/company/update', auth, controller.companyController.update) // 修改子公司
  // router.get('/api/v1/download/list', auth, controller.downloadController.list) // 查询下载资料
  // router.post('/api/v1/download/downFile',app.middleware.auth({ required: false }),controller.downloadController.downFile) // 点击下载文件流
  // router.post('/api/v1/download/insert', auth, controller.downloadController.insert) // 添加下载资料
  // router.post('/api/v1/download/delete', auth, controller.downloadController.delete) // 删除下载资料
  // router.post('/api/v1/download/update', auth, controller.downloadController.update) // 修改下载资料
  // router.get('/api/v1/message/list', auth, controller.messageController.list) // 查询留言列表
  // router.post('/api/v1/message/insert', controller.messageController.insert) // 添加留言
  // router.post('/api/v1/message/delete', auth, controller.messageController.delete) // 删除留言
  // router.post('/api/v1/message/update', auth, controller.messageController.update) // 修改留言
  // router.post('/api/v1/message/reply', auth, controller.messageController.reply) // 回复留言
  // router.post('/api/v1/system/update', auth, controller.systemController.update) // 系统设置
  // router.get('/api/v1/system/list', auth, controller.systemController.list) // 查询系统设置
  router.get('/api/v1/menu/list', auth, controller.menuController.list) // 查询菜单列表
  router.post('/api/v1/menu/insert', auth, controller.menuController.insert) // 添加菜单
  router.post('/api/v1/menu/delete', auth, controller.menuController.delete) // 删除菜单
  router.post('/api/v1/menu/update', auth, controller.menuController.update) // 修改菜单
  // router.get('/api/v1/product/list', app.middleware.auth({ required: false }), controller.productController.list)  // 查询产品列表
  // router.post('/api/v1/product/insert',auth, controller.productController.insert)  // 添加产品列表
  // router.post('/api/v1/product/update',auth, controller.productController.update)  // 修改产品列表
  // router.post('/api/v1/product/delete',auth, controller.productController.delete)  // 删除产品列表
  // router.get('/api/v1/chronicle/list', app.middleware.auth({ required: false }), controller.chronicleController.list)  // 查询大事记列表
  // router.post('/api/v1/chronicle/insert',auth, controller.chronicleController.insert)  // 添加大事记
  // router.post('/api/v1/chronicle/update',auth, controller.chronicleController.update)  // 修改大事记
  // router.post('/api/v1/chronicle/delete',auth, controller.chronicleController.delete)  // 删除大事记
  // router.get('/api/v1/video/list', app.middleware.auth({ required: false }), controller.videoController.list)  // 查询视频列表
  // router.post('/api/v1/video/insert',auth, controller.videoController.insert)  // 添加视频
  // router.post('/api/v1/video/update',auth, controller.videoController.update)  // 修改视频
  // router.post('/api/v1/video/delete',auth, controller.videoController.delete)  // 删除视频
  // router.get('/api/v1/contactWay/list', app.middleware.auth({ required: false }), controller.contactWayController.list)  // 查询联系方式
  // router.post('/api/v1/contactWay/insert',auth, controller.contactWayController.insert)  // 添加联系方式
  // router.post('/api/v1/contactWay/update',auth, controller.contactWayController.update)  // 修改联系方式
  // router.post('/api/v1/contactWay/delete',auth, controller.contactWayController.delete)  // 删除联系方式
};