module.exports = (options = { required: true }) => {
  return async (ctx, next) => {
    // 1. 获取请求头中的 token 数据
    const token = ctx.cookies.get('token')
    if (token) {
      try {
        //  token 有效，根据 userId 获取用户数据挂载到 ctx 对象中给后续中间件使用
        const data = ctx.service.userService.verifyToken(token)
        ctx.user = await ctx.model.User.findById(data._id)
      } catch (err) {
        ctx.throw(401)
      }
    } else if (options.required) {
      ctx.throw(401)
    }
    // 4. next 执行后续中间件
    await next()
  }
}
