const baseField = require('./base')

module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema

  const SecondCommentSchema = new Schema({
    replyId: {
      type: mongoose.ObjectId,
      ref: 'Comment'
    },
    userId: { 
      type: mongoose.ObjectId,
      ref: 'User'
    },
    targetId: {
      type: mongoose.ObjectId,
      ref: 'User'
    },
    article: {
      type: mongoose.ObjectId,
      ref: 'Article'
    },
    content: {
      type: String,
      required: true
    },
    ...baseField
  },{versionKey: false}).set('toJSON', { getters: true })
  return mongoose.model('SecondComment', SecondCommentSchema)
}