const baseField = require('./base')

module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema

  const userSchema = new Schema({
    nickname: { // 用户昵称
      type: String,
      default: '匿名用户'
    },
    status: {  //用户状态
      type: Boolean,
      default: true
    },
    username: { // 用户名
      type: String,
      required: true
    },
    password: { // 密码
      type: String,
      select: false, // 查询中不包含该字段
      required: true,
    },
    avatar: {
      type: String,
      default: 'https://image.baidu.com/search/detail?ct=503316480&z=&tn=baiduimagedetail&ipn=d&word=%E5%A4%B4%E5%83%8F%20%E6%98%8E%E6%98%9F%E5%A4%B4%E5%83%8F%20%E5%88%98%E8%AF%97%E8%AF%97&step_word=&ie=utf-8&in=&cl=2&lm=-1&st=-1&hd=&latest=&copyright=&cs=2422623623,233674837&os=2251259691,3381391190&simid=2422623623,233674837&pn=11&rn=1&di=176110&ln=3412&fr=&fmq=1637031177542_R&ic=&s=undefined&se=&sme=&tab=0&width=&height=&face=undefined&is=0,0&istype=2&ist=&jit=&bdtype=0&spn=0&pi=0&gsm=78&objurl=https%3A%2F%2Fgimg2.baidu.com%2Fimage_search%2Fsrc%3Dhttp%253A%252F%252Fwww.qqwmw.com%252Fuploads%252Fimg%252F20200926%252F1601049836119236.png%26refer%3Dhttp%253A%252F%252Fwww.qqwmw.com%26app%3D2002%26size%3Df9999%2C10000%26q%3Da80%26n%3D0%26g%3D0n%26fmt%3Djpeg%3Fsec%3D1639623170%26t%3D1825529ac9fd86ffce953fe8c8c26be1&rpstart=0&rpnum=0&adpicid=0&nojc=undefined&dyTabStr=MCwzLDEsNCw1LDcsMiw2LDgsOQ%3D%3D'
    },
    isManage: {
      type: Boolean,
      default: false
    },
    ...baseField
  },{versionKey: false}).set('toJSON', { getters: true })

  return mongoose.model('User', userSchema)
}
