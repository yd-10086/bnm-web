const baseField = require('./base')

module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema

  const technologySchema = new Schema({
    title: { //标题
      type: String,
      required: true
    },
    titleEn: { // 英文标题
      type: String,
    },
    description: { // 描述
      type: String,
    },
    descriptionEn: { // 英文描述
      type: String,
    },
    content: { // 内容
      type: String,
    },
    contentEn: { // 英文内容
      type: String,
    },
    img: { // 图片链接
      type: mongoose.ObjectId,
      ref: 'File',
    },
    sortNum: { // 排序
      type: Number,
      default: 0
    },
    ...baseField
  }, {
    versionKey: false
  }).set('toJSON', {
    getters: true
  })
  return mongoose.model('Technology', technologySchema)
}