const baseField = require('./base')

module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema

  const companySchema = new Schema({
    img: { 
      type: mongoose.ObjectId,
      ref: 'File'
    },
    title: String,
    titleEn: String,
    address: String,
    addressEn: String,
    linkman: String,
    linkmanEn: String,
    phoneNumber: String || Number,
    lon: String || Number, // 经度
    lat: String || Number, //纬度
    openTime: String,  // 营业时间
    productSeries: String, // 产品系列
    provinceId: String,   // 省份
    cityId: String,       // 城市
    type: {
      type: String || Number, // 1 国内公司 2国外公司  3服务商
      default: 1
    },
    ...baseField
  },{versionKey: false}).set('toJSON', { getters: true })

  return mongoose.model('Company', companySchema)
}