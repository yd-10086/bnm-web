const baseField = require('./base')

module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema

  const provinceSchema = new Schema({
    sourceId: String,
    name: String,
    pinyin: String,
    bianma: String,
    orderby: String || Number,
    parentid: String || Number,
    remark: String,
    ...baseField
  },{versionKey: false}).set('toJSON', { getters: true })

  return mongoose.model('Province', provinceSchema)
}