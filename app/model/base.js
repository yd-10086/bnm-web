const moment = require('moment')

module.exports = {
  createdAt: { // 创建时间
    type: Date,
    default: Date.now,
    get(val){
      return moment(val).format('YYYY-MM-DD HH:mm:ss');
    }
  },
  updatedAt: { // 更新时间
    type: Date,
    default: Date.now,
    get(val){
      return moment(val).format('YYYY-MM-DD HH:mm:ss');
    }
  }
}