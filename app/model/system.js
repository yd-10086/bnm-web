const baseFiled = require('./base')

module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema

  const SystemlSchema = new Schema({
    name: String, // 公司名称
    nameEn: String, // 公司名称
    // signature: {  //个性签名
    //   type: String,
    //   defualt: '这个人很懒...'
    // },
    // avatar: {
    //   type: mongoose.ObjectId,
    //   ref: 'File'
    // },
    // motto: { //座右铭
    //   type: String
    // },
    // aboutMe: {  // 关于我
    //   type: String
    // },
    seoKeywords: {  // seo 关键字
      type: String,
    },
    seoDescription: { // seo 描述
      type: String
    },
    companyProfile: String, //  企业简介
    companyProfileEn: String, //  企业简介
    principal: String,  // 总负责人
    principalEn: String,  // 总负责人
    principalPhone: String || Number ,// 负责人联系方式
    address: String,
    addressEn: String,
    shopLink: String,
    beian: String,  //备案号
    officialAccounts: {  // 公众号二维码
      type: mongoose.ObjectId,
      ref: 'File'
    },
    logo: {
      type: mongoose.ObjectId,
      ref: 'File'
    },
    mobileLogo: {
      type: mongoose.ObjectId,
      ref: 'File'
    },
    partnerImg: {
      type: mongoose.ObjectId,
      ref: 'File'
    },
    ...baseFiled
  },{versionKey: false}).set('toJSON', { getters: true })

  return mongoose.model('System',SystemlSchema)
}