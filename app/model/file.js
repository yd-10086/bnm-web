const baseField = require('./base')

module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema

  const fileSchema = new Schema({
    link: { // 图片链接
      type: String,
      required: true
    },
    type: {
      type: String || Number,  //1 下载资料 2 banner图
      default: null
    },
    jumpLink: String,
    title: String,
    titleEn: String,
    description: String,
    descriptionEn: String,
    sortNum: String || Number,
    ...baseField
  },{versionKey: false}).set('toJSON', { getters: true })
  return mongoose.model('File', fileSchema)
}