const baseField = require('./base')

module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema

  const productSchema = new Schema({
    title: { //标题
      type: String,
      required: true
    },
    titleEn: { // 英文标题
      type: String,
    },
    description: { // 描述
      type: String,
    },
    descriptionEn: { // 英文描述
      type: String,
    },
    content: { // 内容
      type: String,
    },
    contentEn: { // 英文内容
      type: String,
    },
    menuId: { // 所属系列
      type: mongoose.ObjectId,
      ref: 'Menu'
    },
    imgs: { // 产品图片
      type: [
        {
          type: mongoose.ObjectId,
          ref: 'File',
        }
      ],
    },
    isRecommend: { //推荐
      type: Boolean,
      default: false
    },
    ...baseField
  }, {
    versionKey: false
  }).set('toJSON', {
    getters: true
  })
  return mongoose.model('Product', productSchema)
}