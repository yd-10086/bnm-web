const baseField = require('./base')

module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema

  const messageSchema = new Schema({
    menuId: {  //系列ID
      type: mongoose.ObjectId,
      ref: 'Menu'
    },
    productId: {  // 产品ID
      type: mongoose.ObjectId,
      ref: 'Product'
    },
    customerName: String, //用户姓名
    phoneNumber: String || Number, // 手机号码
    province: String, // 省
    city: String, // 市
    region: String, // 区
    companyName: String, // 公司名
    period: String || Number, // 周期 0 1 2 3 4 5
    askFor: String, // 其他要求
    ...baseField
  }, {
    versionKey: false
  }).set('toJSON', {
    getters: true
  })
  return mongoose.model('Message', messageSchema)
}