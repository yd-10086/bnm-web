const baseField = require('./base')

module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema

  const chronicleSchema = new Schema({
    img: { 
      type: mongoose.ObjectId,
      ref: 'File'
    },
    type: {
      type: String || Number,
      default: 1
    }, // 1大事记 2 荣誉
    year: String || Number,
    month: String || Number,
    content: String,
    contentEn: String,
    title: String,
    titleEn: String,
    ...baseField
  },{versionKey: false}).set('toJSON', { getters: true })

  return mongoose.model('Chronicle', chronicleSchema)
}