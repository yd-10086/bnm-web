const baseField = require('./base')

module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema

  const menuSchema = new Schema({
    parentId: { // 父级ID
      type: mongoose.ObjectId,
      ref: 'Menu'
    },
    title: { // 菜单名称
      type: String,
      required: true
    },
    titleEn: { // 菜单英文名称
      type: String,
    },
    type: {
      type: String,
      enum: ['link','menu'],
      required: true
    },
    path: {  // 菜单路由
      type: String,
      required: true
    },
    sortNum: { // 排序
      type: Number,
      default: 0
    },
    description: { // 菜单描述
      type: String,
    },
    descriptionEn: String, // 菜单描述英文
    ...baseField
  },{versionKey: false}).set('toJSON', { getters: true })
  
  return mongoose.model('Menu', menuSchema)
}