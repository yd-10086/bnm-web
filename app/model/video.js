const baseField = require('./base')

module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema

  const videoSchema = new Schema({
    posterUrl: { 
      type: mongoose.ObjectId,
      ref: 'File'
    },
    videoUrl: { 
      type: mongoose.ObjectId,
      ref: 'File'
    },
    type: {
      type: String || Number,  // 1 视频 2 情怀图片 3 企业理念
      default: null
    },
    title: String,
    titleEn: String,
    description: String,
    descriptionEn: String,
    sortNum: {
      type: Number || String,
      default: 0
    },
    ...baseField
  },{versionKey: false}).set('toJSON', { getters: true })

  return mongoose.model('Video', videoSchema)
}