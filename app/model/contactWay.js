const baseField = require('./base')

module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema

  const contactWaySchema = new Schema({
    title: String,
    titleEn: String,
    subTitle1: String,
    subTitle1En: String,
    subTitle2: String,
    subTitle2En: String,
    subTitle1Phone: String || Number,
    subTitle2Phone: String || Number,
    sortNum: { // 排序
      type: Number,
      default: 0
    },
    ...baseField
  },{versionKey: false}).set('toJSON', { getters: true })
  
  return mongoose.model('ContactWay', contactWaySchema)
}