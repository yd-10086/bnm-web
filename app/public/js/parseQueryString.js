function parseQueryString(url) {
  var obj = {};
  var keyvalue = [];
  var key = "",
    value = "";
  if (url.indexOf('?') != -1) {
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    for (var i in paraString) {
      keyvalue = paraString[i].split("=");
      key = keyvalue[0];
      value = keyvalue[1];
      obj[key] = value;
    }
    return obj;
  } else {
    return undefined
  }
}

function xhrRequest(api,url,callback) {
  var arrs = url.split('/')
  var filename = arrs[arrs.length-1]
  var xhr = new XMLHttpRequest();
  xhr.open('POST',api );
  xhr.withCredentials = true;
  xhr.responseType = "blob";
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send('url='+url);
  xhr.onload = function () {
    if (this.status == 200) {
      callback()
      downloadFile(xhr.response,filename)
    }
  }
}

function downloadFile(res, fileName) { //res为返回的文件流，fileName为文件命名
  if (!res) {
    return
  }
  // let suffix = fileName.split('.')[1]
  // let type = 'application/octet-stream'
  //let type = 'application/'+suffix+';chartset=UTF-8' // 这里如果是其他格式的文件，把pdf改为所下载的文件格式就可以
  const blob = new Blob([res],{type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
  if ('download' in document.createElement('a')) { // 非IE下载
    let url = URL.createObjectURL(blob)
    let link = document.createElement('a')
    link.style.display = 'none'
    link.href = url
    link.setAttribute('download', fileName)
    document.body.appendChild(link)
    link.click()
    URL.revokeObjectURL(link.href) // 释放URL 对象
    document.body.removeChild(link)
  } else { // IE10+下载
    navigator.msSaveBlob(blob, fileName)
  }
}

// 根据链接下载文件
function downLoadFileUrl (url) {
  const link = document.createElement('a')
  link.style.display = 'none'
  link.href = url
  link.target = '_blank'
  document.body.appendChild(link)
  link.click()
  document.body.removeChild(link)
}