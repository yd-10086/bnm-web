const Service = require('egg').Service

class MenuService extends Service {
  get Menu() {
    return this.ctx.model.Menu
  }
  // 插入菜单
  async insert(data) {
    const _data = await new this.Menu(data)
    _data.save()
    return _data
  }

  // 查询菜单
  async getList() {
    const ret = await this.Menu.find({},null,{
      sort: { sortNum: -1 }
    }).populate('menuImg', 'link').populate('seriesImg','link').populate('mobileMenuImg','link')
    const newData = JSON.parse(JSON.stringify(ret))
    return this.ctx.service.toolService.buildTree(newData,'parentId','_id')
  }
}

module.exports = MenuService