const Service = require('egg').Service

class commentService extends Service {
  get Company() {
    return this.ctx.model.Company
  }
  
  // 插入子公司
  async insert(data) {
    const _data = await new this.Company(data)
    _data.save()
    return _data
  }

  // 查询子公司
  async getList(body) {
    const pageNum = Number.parseInt(body.pageNum || 1)
    const pageSize = Number.parseInt(body.pageSize || 10)
    const titleReg = new RegExp(body.title,'ig')
    const params = {}
    if(body.title) {
      params.title = titleReg
    }
    if(body.type) {
      params.type = body.type
    }
    const ret = await this.Company.find(params).skip((pageNum-1)*pageSize).limit(pageSize).populate('img','link')
    const total = await this.Company.countDocuments(params)
    return {
      list: ret,
      total
    }
  }


  // 删除子公司
  async delete(id) {
    const { ctx,service } = this
    const _data = await this.Company.findById(id).populate('img','link')
    if(!_data) {
      ctx.throw('_id不正确！')
    }
    _data.remove()
    //删除文件
    if(_data.img) {
      await ctx.model.File.deleteOne({_id:_data.img._id})
      service.toolService.deleteDiskFile(_data.img.link)
    }
    
    return {
      code: 200,
      message: '删除成功'
    }
  }

  // 修改子公司
  async update(body) {
    const { ctx } = this
    //查找子公司
    const _data = await this.Company.findByIdAndUpdate(body.id,Object.assign(body,{updatedAt: Date.now()})) 
    if(!_data) {
      ctx.throw(422,'_id不正确！')
    }
    return {
      code: 200,
      message: '修改成功'
    }
  }
}

module.exports = commentService