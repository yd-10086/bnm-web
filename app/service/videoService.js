const Service = require('egg').Service

class videoService extends Service {
  get Video() {
    return this.ctx.model.Video
  }
  
  // 新增
  async insert(data) {
    const _data = await new this.Video(data)
    _data.save()
    return _data
  }

  // 查询
  async getList(body) {
    const pageNum = Number.parseInt(body.pageNum || 1)
    const pageSize = Number.parseInt(body.pageSize || 10)
    const titleReg = new RegExp(body.title,'ig')
    let params = {}
    if(body.title) {
      params.title = titleReg
    }
    if(body.type) {
      params.type = body.type
    }
    const ret = await this.Video.find(params,null,{
      sort: {sortNum: -1}
    }).skip((pageNum-1)*pageSize).limit(pageSize).populate('posterUrl','link').populate('videoUrl','link')
    const total = await this.Video.countDocuments(params)
    return {
      list: ret,
      total
    }
  }

  // 删除
  async delete(id) {
    const { ctx,service } = this
    const _data = await this.Video.findById(id).populate('posterUrl','link').populate('videoUrl','link')
    if(!_data) {
      ctx.throw('_id不正确！')
    }
     //删除文件
    _data.remove()
    if(_data.posterUrl) {
      await ctx.model.File.deleteOne({_id:_data.posterUrl._id})
      service.toolService.deleteDiskFile(_data.posterUrl.link)
    }
    if(_data.videoUrl) {
      await ctx.model.File.deleteOne({_id:_data.videoUrl._id})
      service.toolService.deleteDiskFile(_data.videoUrl.link)
    }
    return {
      code: 200,
      message: '删除成功'
    }
  }

  // 修改
  async update(body) {
    const { ctx } = this
    const _data = await this.Video.findByIdAndUpdate(body.id,Object.assign(body,{updatedAt: Date.now()})) 
    if(!_data) {
      ctx.throw(422,'_id不正确！')
    }
    return {
      code: 200,
      message: '修改成功'
    }
  }
  
}

module.exports = videoService