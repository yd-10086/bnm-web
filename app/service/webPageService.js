const Service = require('egg').Service

class webPageService extends Service {

  // 菜单 model
  get MenuModel() {
    return this.ctx.model.Menu
  }
  // 产品model
  get ProductModel() {
    return this.ctx.model.Product
  }
  //留言表
  get MessageModel() {
    return this.ctx.model.Message
  }
  // 技术支持表
  get Technology() {
    return this.ctx.model.Technology
  }
  // 子公司表
  get Company() {
    return this.ctx.model.Company
  } 
  // 文件表
  get File() {
    return this.ctx.model.File
  }
  // 视频 照片 理念
  get Video() {
    return this.ctx.model.Video
  }
  // 大事记 荣誉、
  get Chronicle() {
    return this.ctx.model.Chronicle
  }

  // 当前菜单
  async getCurrentMenu(params) {
    return await this.MenuModel.findById(params.id).select('title')
  }

  // 列表
  async getProductList(params) {
    const pageNum = Number.parseInt( params.pageNum || 1 )
    const pageSize = Number.parseInt( params.pageSize || 8 )
    const _filter = {}
    if(params.id) {
      _filter.menuId = params.id
    }
    const productData = await this.ProductModel.find(_filter).populate('imgs', 'link').skip((pageNum - 1) * pageSize).limit(pageSize)
    const total = await this.ProductModel.countDocuments(_filter)
    return {
      productData,
      total
    }
  }

  // 详情
  async getProductDetail(_id) {
    const data = await this.ProductModel.findById(_id).populate('imgs', 'link')
    const _otherData = await this.ProductModel.find({
      menuId: data.menuId,
      _id: {
        $ne: _id
      }
    }).populate('imgs', 'link').limit(4)
    return {
      data,
      _otherData
    }
  }

  // 用户留言
  async inserUserMessage(body) {
    const data = await new this.MessageModel(body).save()
    return data
  }

  // 技术支持列表
  async getTechnologyList(params) {
    const pageNum = Number.parseInt(params.pageNum || 1)
    const pageSize = Number.parseInt(params.pageSize || 4)
 
    const ret = await this.Technology.find({},null,{
      sort: {sortNum: -1}
    }).skip((pageNum-1)*pageSize).limit(pageSize).populate('img', 'link')

    const total = await this.Technology.countDocuments()
    return {
      list: ret,
      total
    }
  }

  // 获取banner列表
  async getBannerList() {
    return await this.File.find({type: 2},null,{sort: {sortNum: -1}})
  }

  // 获取技术支持详情
  async getInfoDetail(params) {
    return await this.Technology.findById(params.id).populate('img','link')
  }

  // 获取全球战略子公司名字
  async getGlobalCompany(params) {
    const pageNum = Number.parseInt(params.pageNum || 1)
    const pageSize = Number.parseInt(params.pageSize || 6)
    
    const ret = await this.Company.find({type: 1},null,{
      sort: {sortNum: -1}
    }).skip((pageNum-1)*pageSize).limit(pageSize).populate('img', 'link')

    const total = await this.Company.countDocuments()
    return {
      list: ret,
      total
    }
  }

  // 获取所有带位置的子公司
  async getAllPointCompany() {
    return await this.Company.find({
      lon: { $exists: true },
      lat: { $exists: true }
    }).populate('img','link')
  }

  // 获取下载列表
  async getFileList(params) {
    const pageNum = Number.parseInt(params.pageNum || 1)
    const pageSize = Number.parseInt(params.pageSize || 999)
    const list = await this.File.find({type: 1},null,{
      sort: {sortNum: -1}
    }).skip((pageNum-1)*pageSize).limit(pageSize)
    const total = await this.File.countDocuments({type: 1})
    
    return {
      list,
      total
    }
  }

  // 获取所有大事记
  async getChronicleList(params) {
    const value = Number.parseInt(params.value || 7 ) 
    const data = await this.Chronicle.find({type: 1},null,{
      sort: {year: -1}
    }).populate('img','link')
    // 分组
    const list = []
    data.forEach((item,index) => {
      // 如果数组长度为0 直接push
      if(!list.length) {
        list.push({year: item.year,list: [item]})
      }else {
        // 如果数组长度不为0 先判断是否存在当前年
        const index = list.findIndex(l => l.year == item.year)
        if(index == -1) {
          list.push({year: item.year,list: [item]})
        } else {
          list[index].list.push(item)
        }
      }
    })

    let result = []
    list.forEach((item,index) => {
      const _i = Math.floor( index / value )
      if(!result[_i]) result[_i] = []
      result[_i].push(item)
    })
    return result
  }

  // 根据年份查找大事记
  async getChronicleByYear(year) {
    return await this.Chronicle.find({year,type:1}).populate('img')
  }

  // 获取所有视频
  async getVideoList(params) {
    const pageNum = Number.parseInt(params.pageNum || 1)
    const pageSize = Number.parseInt(params.pageSize || 6)
    const firstVideo = await this.Video.find({type: 1}).limit(1).populate('posterUrl','link').populate('videoUrl','link')
    const list = await this.Video.find({type:1},null,{
      sort: {sortNum: -1}
    }).skip((pageNum-1)*pageSize+1).limit(pageSize).populate('posterUrl','link').populate('videoUrl','link')
    const total = await this.Video.countDocuments({type:1}) -1

    return {
      total,
      list,
      firstVideo
    }
  }

  // 获取所有企业理念
  async getPhilosophy() {
    return await this.Video.find({type: 3}).populate('posterUrl','link')
  }

  // 获取所有情怀
  async getFeelings() {
    const _data = await this.Video.find({type: 2}).populate('posterUrl','link')
    let result = []
    _data.forEach((item,index) => {
      const _i = Math.floor( index / 3 )
      if(!result[_i]) result[_i] = []
      result[_i].push(item)
    })
    return result
  }

  //获取所有荣誉
  async getHonorData() {
    const data = await this.Chronicle.find({type: 2},null,{
      sort: {year: -1}
    }).populate('img','link')
    // 分组
    const list = []
    data.forEach((item,index) => {
      // 如果数组长度为0 直接push
      if(!list.length) {
        list.push({year: item.year,list: [item]})
      }else {
        // 如果数组长度不为0 先判断是否存在当前年
        const index = list.findIndex(l => l.year == item.year)
        if(index == -1) {
          list.push({year: item.year,list: [item]})
        } else {
          list[index].list.push(item)
        }
      }
    })

    let result = []
    list.forEach((item,index) => {
      const _i = Math.floor( index / 7 )
      if(!result[_i]) result[_i] = []
      result[_i].push(item)
    })
    return result
  }

  // 根据年份获取荣誉
  async getHonorByYear(year) {
    return await this.Chronicle.find({year,type:2}).populate('img')
  }

  // 获取联系方式列表
  async getContactList() {
    return await this.ctx.model.ContactWay.find()
  }

  //获取视频详情
  async getVideoDetail(id) {
    return await this.Video.findById(id).populate('posterUrl','link').populate('videoUrl','link')
  }

  // 获取销售商
  async getSalesList(query) {
    const { ctx } = this
    const salesModel = ctx.model.Company 
    const filter = {
      type: 3
    }
    if(query.provinceId) {
      filter.provinceId = query.provinceId
    }
    if(query.cityId) {
      filter.cityId = query.cityId
    }
    if(query.productSeries) {
      filter.productSeries = query.productSeries
    }
    const data = await salesModel.find(filter).populate('img','link')
    return data
  }
}

module.exports = webPageService