const Service = require('egg').Service

class messageService extends Service {
  get Message() {
    return this.ctx.model.Message
  }
  // 插入留言
  async insert(data) {
    const _data = await new this.Message(data)
    _data.save()
    return _data
  }

  // 查询留言列表
  async list(body) {
    const pageNum = Number.parseInt(body.pageNum || 1)
    const pageSize = Number.parseInt(body.pageSize || 10)
    const ret = await this.Message.find().skip((pageNum-1)*pageSize).limit(pageSize).populate('menuId','title').populate('productId','title')
    const total = await this.Message.countDocuments()
    return {
      list: ret,
      total
    }
  }
}

module.exports = messageService