const Controller = require('egg').Controller

/**
 * @Controller 系统设置
 */
class systemController extends Controller {
  /**
   * @summary 更新系统设置
   * @router post /system/update
   ** @request body systemRequest
   * @response 200 testResponse
   * @apikey
 */
  async update() {
    const { ctx } = this
    const System = ctx.model.System
    const body = ctx.request.body
    const findData = await System.find()
    let ret
    if(!findData.length) {
      ret = await new System(body).save()
    }else {
      ret = await System.updateOne({_id: findData[0]._id},body)
    }
    if(!ret) {
      ctx.throw(422, '修改出错')
    }
    const data = await System.find().populate('logo','link').populate('mobileLogo','link').populate('partnerImg','link').populate('officialAccounts','link')
  

    ctx.body = {
      code: 200,
      message: '修改成功'
    }
  }
    /**
   * @summary 查询系统设置信息
   * @router get /system/list
   * @response 200 testResponse
   * @apikey
 */
  async list() {
    const data = await this.ctx.model.System.find().populate('officialAccounts','link').populate('logo','link').populate('mobileLogo','link').populate('partnerImg','link')
    this.ctx.body = {
      code: 200,
      data: data.length ? data[0] : null
    }
  }
}

 module.exports = systemController