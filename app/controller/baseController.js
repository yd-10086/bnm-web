const Controller = require('egg').Controller

class BaseController extends Controller {
	
	/**
	 * 获取菜单列表
	 */
 	async getMenuList() {
		const { service }  = this
    const menus = await service.menuService.getList()
		return menus
	}

	/**
	 * 获取所有标签
	 * @params 需要返回的值
	 */
	async getSortArticleList(filter,params) {
		const ArticleModel = this.ctx.model.Article
		// 查询文章按阅读量排序
    const data = await ArticleModel.find({},null,filter).select(params).limit(8)
		return data
	}
	/**
	 * 获取博客配置信息
	 * @params 需要返回的值
	 */
	async getSystemInfo() {
		return await this.ctx.model.System.find().populate('logo','link').populate('officialAccounts','link').populate('mobileLogo','link').populate('partnerImg','link')
	}

	/**
	 * 获取博客公共数据
	 */
  async getCommonData() {
    let menu,systemInfo
    // 获取菜单
		menu = await this.getMenuList()
    // 获取用户信息
		systemInfo = await this.getSystemInfo()
    //返回结果
    return {
      menu,
			systemInfo: systemInfo[0]
    }
  }
}

module.exports = BaseController