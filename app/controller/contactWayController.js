const Controller = require('egg').Controller
/**
 * @Controller 联系方式
 */
class contactWayController extends Controller {

  /**
   * @summary 添加联系方式
 */
  async insert() {
    const { ctx, service } = this
    const body = ctx.request.body
    ctx.validate({
      title: 'string',
    },body)
    const data = await service.contactWayService.insert(body)
    ctx.body = {
      code: 200,
      data,
      message: '添加成功'
    }
  }

  /**
   * @summary 删除联系方式
 */
  async delete() {
    const { ctx,service } = this
    const body = ctx.request.body
    ctx.validate({
      id: 'string'
    },body)
    const data = await service.contactWayService.delete(body.id)
    ctx.body = data
  }

  /**
   * @summary 修改联系方式
 */
   async update() {
    const { ctx,service } = this
    const body = ctx.request.body
    ctx.validate({
      id: 'string',
      title: 'string',
    },body)
    const data = await service.contactWayService.update(body)
    ctx.body = {
      code: 200,
      data,
      message: '修改成功'
    }
  }

  /**
  * @summary 获取联系方式列表
 */
  async list() {
    const { ctx,service } = this
    const body = ctx.request.query
    ctx.validate({
      pageNum: 'string'
    },body)
    const data = await service.contactWayService.list(body)
    ctx.body = {
      code: 200,
      data
    }
  }
}

module.exports = contactWayController