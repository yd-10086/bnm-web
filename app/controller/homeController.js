const BaseController = require('./baseController')

/**
 * @class HomerController
 * @extends {BaseController}
 */
class HomerController extends BaseController {

  // 首页
  async index() {
    const {
      ctx
    } = this
    const bannerData = await ctx.service.webPageService.getBannerList()

    // 获取技术支持4条数据
    const technologyData = await ctx.service.webPageService.getTechnologyList({})
    // 获取公共数据
    // const _commonData = await this.getCommonData()
    const systemData = {}
    await ctx.render('index.html', {
      systemData
      // ..._commonData,
      // technologyData,
      // bannerData
    })
  }


  // 关于页面
  async about() {
    const {
      ctx
    } = this
    await ctx.render('about.html', {

    })
  }

  //研发创新
  async innovate() {
    const {
      ctx
    } = this
    await ctx.render('innovate.html', {

    })
  }

  //联系我们
  async contactUs() {
    const {
      ctx
    } = this
    await ctx.render('contactUs.html', {

    })
  }

  //产品页
  async products() {
    const {
      ctx
    } = this
    await ctx.render('products.html', {

    })
  }

  //产品详情页
  async products() {
    const {
      ctx
    } = this
    await ctx.render('product-details.html', {

    })
  }



  // 产品列表页面
  async productList() {
    const {
      ctx,
      service
    } = this
    const params = ctx.request.query

    await ctx.render('product-list.html', {

    })
  }

  // 产品详情页
  async productDetail() {
    const {
      ctx,
      service
    } = this

    await ctx.render('product-details.html', {

    })
  }



  // 解决方案首页
  async program() {
    const {
      ctx,
      service
    } = this

    await ctx.render('program.html', {

    })
  }

  // 解决方案详情页
  async programDetail() {
    const {
      ctx,
      service
    } = this

    await ctx.render('program-details.html', {

    })
  }

  // 服务支持页
  async support() {
    const {
      ctx,
      service
    } = this
    await ctx.render('support.html', {

    })
  }

  // 网上留言页
  async message() {
    const {
      ctx,
      service
    } = this
    await ctx.render('message.html', {

    })
  }

  // 资料下载页面
  async download() {
    const {
      ctx,
      service
    } = this
    await ctx.render('download.html', {

    })
  }

  // 售后服务页
  async afterSale() {
    const {
      ctx,
      service
    } = this
    await ctx.render('after-sale.html', {

    })
  }

  // 满意度调研页
  async serveSurvey() {
    const {
      ctx,
      service
    } = this
    await ctx.render('serve-survey.html', {

    })
  }

  // 数字未来页
  async numberFuture() {
    const {
      ctx,
      service
    } = this
    await ctx.render('number-future.html', {

    })
  }

  // 新闻页面
  async news() {
    const {
      ctx,
      service
    } = this
    await ctx.render('news.html', {

    })
  }

  //视频中心页

  async videoCenter() {
    const {
      ctx,
      service
    } = this
    await ctx.render('video-center.html', {

    })
  }

  //公司公告页
  async notice() {
    const {
      ctx,
      service
    } = this
    await ctx.render('notice.html', {

    })
  }
}

module.exports = HomerController