const Controller = require('egg').Controller
/**
 * @Controller 技术支持
 */
class technologyController extends Controller {

  /**
   * @summary 添加技术支持
   */
  async insert() {
    const {
      ctx,
      service
    } = this
    const body = ctx.request.body
    ctx.validate({
      title: 'string',
      description: 'string',
      content: 'string'
    }, body)
    await service.technologyService.insert(body)

    ctx.body = {
      code: 200,
      message: '添加成功'
    }
  }

  /**
   * @summary 删除技术支持
   */
  async delete() {
    const {
      ctx,
      service
    } = this
    const body = ctx.request.body
    ctx.validate({
      id: 'string'
    }, body)

    const data = await service.technologyService.delete(body.id)
    ctx.body = data
  }

  /**
   * @summary 修改技术支持
   */
  async update() {
    const {
      ctx,
      service
    } = this
    const body = ctx.request.body
    ctx.validate({
      id: 'string',
      title: 'string',
      description: 'string',
      content: 'string'
    }, body)
    const data = await service.technologyService.update(body)

    ctx.body = data
  }

  /**
   * @summary 获取技术支持列表
   */
  async list() {
    const {
      ctx,
      service
    } = this
    const body = ctx.request.query

    const data = await service.technologyService.getList(body)
    ctx.body = {
      code: 200,
      data
    }
  }
}

module.exports = technologyController