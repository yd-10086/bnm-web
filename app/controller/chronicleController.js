const Controller = require('egg').Controller

/**
 * @Controller 大事记
 */
class chronicleController extends Controller {

  /**
   * @summary 添加
 */
  async insert() {
    const { ctx, service } = this
    const body = ctx.request.body
    ctx.validate({
      year: 'number',
      month: 'number'
    },body)

    await service.chronicleService.insert(body)
    ctx.body = {
      code: 200,
      message: '添加成功'
    }
  }

  /**
   * @summary 删除
 */
  async delete() {
    const { ctx,service } = this
    const body = ctx.request.body
    ctx.validate({
      id: 'string'
    },body)
    const data = await service.chronicleService.delete(body.id)

    ctx.body = data
  }

  /**
   * @summary 修改
 */
   async update() {
     
    const { ctx,service } = this
    const body = ctx.request.body
    ctx.validate({
      id: 'string',
    },body)
    const data = await service.chronicleService.update(body)
    
    ctx.body = data
  }

  /**
  * @summary  查询
 */
  async list() {
    const { ctx,service } = this
    const body = ctx.request.query
    ctx.validate({
      pageNum: 'string'
    },body)
    const data = await service.chronicleService.getList(body)
    ctx.body = {
      code: 200,
      data
    }
  }


}

module.exports = chronicleController