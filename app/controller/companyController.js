const Controller = require('egg').Controller
/**
 * @Controller 子公司
 */
class companyController extends Controller {

  /**
   * @summary 添加子公司
 */
  async insert() {
    const { ctx, service } = this
    const body = ctx.request.body
    ctx.validate({
      title: 'string',
      address: 'string'
    },body)

    await service.companyService.insert(body)
    ctx.body = {
      code: 200,
      message: '添加成功'
    }
  }

  /**
   * @summary 删除子公司
 */
  async delete() {
    const { ctx,service } = this
    const body = ctx.request.body
    ctx.validate({
      id: 'string'
    },body)
    const data = await service.companyService.delete(body.id)
    ctx.body = data
  }

  /**
   * @summary 修改子公司
 */
   async update() {
    const { ctx,service } = this
    const body = ctx.request.body
    ctx.validate({
      id: 'string',
      title: 'string',
      address: 'string'
    },body)
    const data = await service.companyService.update(body)
    
    ctx.body = data
  }

  /**
  * @summary 获取子公司列表
 */
  async list() {
    const { ctx,service } = this
    const body = ctx.request.query
    ctx.validate({
      pageNum: 'string'
    },body)
    const data = await service.companyService.getList(body)
    ctx.body = {
      code: 200,
      data,
    }
  }
}

module.exports = companyController