module.exports = app => {
	const store = {}
	app.sessionStore = {
		async get(key) {
			return store[key]
		},
		async set(key, value, maxAge) {
			store[key] = value
			setTimeout(() => {
				this.destroy(key)
			},maxAge)
		},
		async destroy(key) {
			store[key] = null
		}
	}
}